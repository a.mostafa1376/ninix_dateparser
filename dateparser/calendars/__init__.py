# coding: utf-8

from __future__ import unicode_literals
from dateparser.parser import _parser
from dateparser.conf import settings
from datetime import datetime
import re


class CalendarBase(object):
    """Base setup class for non-Gregorian calendar system.

    :param source:
        Date string passed to calendar parser.
    :type source: str|unicode
    """

    parser = NotImplemented

    def __init__(self, source):
        self.source = source

    def get_date(self):
        try:
            output = self.parser.parse(self.source, settings)
            string_without_time  = output[0]
            string_without_time = ' '.join(string_without_time.split())
            date_obj, period  = output[1]
            return {'string_without_time': string_without_time, 'date_obj': date_obj, 'period': period}
        except ValueError:
            pass


class non_gregorian_parser(_parser):

    calendar_converter = NotImplemented
    default_year = NotImplemented
    default_month = NotImplemented
    default_day = NotImplemented
    non_gregorian_date_cls = NotImplemented

    _digits = None
    _months = None
    _weekdays = None
    _number_letters = None

    @classmethod
    def _replace_time_conventions(cls, source):
        return source

    @classmethod
    def _replace_digits(cls, source):
        return source

    @classmethod
    def _replace_months(cls, source):
        return source

    @classmethod
    def _replace_weekdays(cls, source):
        return source

    @classmethod
    def _replace_time(cls, source):
        return source

    @classmethod
    def _replace_days(cls, source):
        return source

    @classmethod
    def _replace_non_latin_words(cls, source):
        string_without_time = re.sub(u'[\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u' ', source)
        source = re.sub(u'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', source)
        return source, string_without_time

    @classmethod
    def to_latin(cls, source):
        result = source
        result = cls._replace_months(result)
        result = cls._replace_weekdays(result)
        result = cls._replace_digits(result)
        result = cls._replace_days(result)
        result = cls._replace_time(result)
        result = cls._replace_time_conventions(result)
        result, string_without_time = cls._replace_non_latin_words(result)

        result = result.strip()
        if result.split()[-1].isdigit():
            tokens = result.split()
            tokens[-1] = tokens[-1] + ':00'
            result = ' '.join(tokens)
        return result, string_without_time

    def _get_datetime_obj(self, **params):
        day = params['day']
        year = params['year']
        month = params['month']
        if (
            not(0 < day <= self.calendar_converter.month_length(year, month)) and
            not(self._token_day or hasattr(self, '_token_weekday'))
        ):
            day = self.calendar_converter.month_length(year, month)
        year, month, day = self.calendar_converter.to_gregorian(year=year, month=month, day=day)
        c_params = params.copy()
        c_params.update(dict(year=year, month=month, day=day))
        return datetime(**c_params)

    def _get_datetime_obj_params(self):
        if not self.now:
            self._set_relative_base()
        now_year, now_month, now_day = self.calendar_converter.from_gregorian(
                self.now.year, self.now.month, self.now.day)
        params = {
            'day': self.day or now_day,
            'month': self.month or now_month,
            'year': self.year or now_year,
            'hour': 0, 'minute': 0, 'second': 0, 'microsecond': 0,
        }
        return params

    def _get_date_obj(self, token, directive):
        year, month, day = self.default_year, self.default_month, self.default_day
        token_len = len(token)
        is_digit = token.isdigit()
        if directive == '%A' and self._weekdays and token.title() in self._weekdays:
            pass
        elif (
            directive == '%m' and
            token_len <= 2 and
            is_digit and
            1 <= int(token) <= 12
        ):
            month = int(token)
        elif directive == '%B' and self._months and token in self._months:
            month = list(self._months.keys()).index(token) + 1
        elif (
            directive == '%d' and
            token_len <= 2 and
            is_digit and
            0 < int(token) <= self.calendar_converter.month_length(year, month)
        ):
            day = int(token)
        elif directive == '%Y' and token_len == 4 and is_digit:
            year = int(token)
        else:
            raise ValueError
        return self.non_gregorian_date_cls(year, month, day)

    @classmethod
    def parse(cls, datestring, settings):
        datestring, string_without_time = cls.to_latin(datestring)
        return string_without_time, super(non_gregorian_parser, cls).parse(datestring, settings)
